//
//  Base.swift
//  boond
//
//  Created by Lassana SYLLA on 2/4/17.
//  Copyright © 2017 Lassana SYLLA. All rights reserved.
//

import Foundation
import UIKit

struct data {
    var name:       String
    var date:       String
    var descript:   String
}

var tab = [data]()

class base: UITableViewController {
    
    
    @IBOutlet weak var done: UITableView!
   
    // Chargement et Refresh de la View en automatique
    
    override func viewDidLoad() {
        tab = [data(name: "Maxime", date: "22 octobre 2010", descript: "kira se promene dans les bois pendant que lighto maitrise le coupable, maxime tombe de la falaise avant de tout avouer, dommage pour L :) !"),
        data(name: "jackie", date: "02 decembre 1998", descript: "il suffit de trois methode principale pour creer une tableview tout d'abord definir une structure d'elements, puis mettre en place un refresh automatique ViewDidLoad")]
        tab.append(data(name: "dark vador", date: "01 janvier 1999", descript: "coup de sabre laser"))
        
        self.tableView.rowHeight = 110 // intialisation par defaut de la taille de la ROW
    }
    
    // Compte le nombre d'element dans le tableau de structure
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tab.count
    }
    
    // Creation et initialisation des valeur d'une cellule
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let Cellul = Bundle.main.loadNibNamed("myCell", owner: self, options: nil)?.first as! myCell
        
        Cellul.date.text = tab[indexPath.row].date
        Cellul.name.text = tab[indexPath.row].name
        Cellul.descript.text = tab[indexPath.row].descript
        
        return Cellul
    }
    
    // initialisation de la Row en responsive
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func viewDidAppear(_ animated: Bool) {
        done.reloadData()
    }
    
    @IBAction func returnBase(segue: UIStoryboardSegue)
    {
        
    }
}
