//
//  myCell.swift
//  boond
//
//  Created by Lassana SYLLA on 2/4/17.
//  Copyright © 2017 Lassana SYLLA. All rights reserved.
//

import UIKit

class myCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var descript: UILabel!
    
}
